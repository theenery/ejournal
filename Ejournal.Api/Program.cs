using Ejournal.Application;
using Ejournal.Infrastructure;
using FirebaseAdmin;
using Google.Api;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Authentication;

namespace Ejournal.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            var firebaseAppOptions = new AppOptions()
            {
                Credential = GoogleCredential.FromFile("firebase-key.json")
            };
            FirebaseApp.Create(firebaseAppOptions);

            builder.Services.AddControllers();
            
            builder.Services.AddSwaggerGen();

            builder.Services.AddInfrastructure();

            builder.Services.AddApplication();

            builder.Services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CustomAuthenticationSchemes.Firebase;
                options.DefaultChallengeScheme = CustomAuthenticationSchemes.Firebase;
            })
            .AddScheme<AuthenticationSchemeOptions, FirebaseAuthenticationHandler>(CustomAuthenticationSchemes.Firebase, null);

            builder.Services.AddAuthorization();

            var app = builder.Build();

            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseMiddleware<ExceptionHandlingMiddleware>();

            app.MapControllers();

            app.Run();
        }
    }
}
