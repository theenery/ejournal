﻿using FirebaseAdmin.Auth;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.Text.Encodings.Web;

namespace Ejournal.Api
{
    public class FirebaseAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        public FirebaseAuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            string authorizationHeader = Request.Headers["Authorization"];

            if (string.IsNullOrEmpty(authorizationHeader) || !authorizationHeader.StartsWith("Bearer "))
            {
                return AuthenticateResult.NoResult();
            }

            string token = authorizationHeader.Substring("Bearer ".Length).Trim();

            // Validate the token with Firebase
            FirebaseToken? firebaseToken;
            try
            {
                firebaseToken = await FirebaseAdmin.Auth.FirebaseAuth.DefaultInstance.VerifyIdTokenAsync(token);
            }
            catch (Exception)
            {
                return AuthenticateResult.Fail("Invalid Firebase token.");
            }

            if (firebaseToken == null)
            {
                return AuthenticateResult.Fail("Invalid Firebase token.");
            }

            var claims = new[] { new Claim(ClaimTypes.Name, firebaseToken.Uid) };
            var identity = new ClaimsIdentity(claims, CustomAuthenticationSchemes.Firebase);
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, CustomAuthenticationSchemes.Firebase);

            return AuthenticateResult.Success(ticket);
        }
    }

}