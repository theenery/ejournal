﻿using System.Net;

namespace Ejournal.Api
{
    public class ExceptionHandlingMiddleware(RequestDelegate next)
    {
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception e)
            {
                await HandleExceptionAsync(context, e);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            ExceptionResponse response = exception switch
            {
                InvalidOperationException => new ExceptionResponse(
                    HttpStatusCode.BadRequest,
                    "Application exception occurred."),
                UnauthorizedAccessException => new ExceptionResponse(
                    HttpStatusCode.Unauthorized,
                    "Unauthorized."),
                _ => new ExceptionResponse(
                    HttpStatusCode.InternalServerError,
                    "Internal server error. Please retry later.")
            };

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)response.StatusCode;
            await context.Response.WriteAsJsonAsync(response);
        }

        public record ExceptionResponse(HttpStatusCode StatusCode, string Description);
    }
}
