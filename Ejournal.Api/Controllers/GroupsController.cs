﻿using Ejournal.Application.Dtos;
using Ejournal.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Ejournal.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class GroupsController(IGroupService groupService) : ControllerBase
    {
        [HttpPost]
        public async Task<ActionResult<GroupDto>> CreateGroup(CreateGroupDto groupDto)
        {
            var uid = User.FindFirst(ClaimTypes.Name)?.Value;
            if (string.IsNullOrEmpty(uid))
            {
                return Unauthorized();
            }

            var group = await groupService.CreateGroupAsync(groupDto, uid);
            return CreatedAtAction(nameof(GetGroup), new { groupId = group.Id }, group);
        }

        [HttpGet("{groupId}")]
        public async Task<ActionResult<GroupDto>> GetGroup([FromRoute] string groupId)
        {
            var group = await groupService.GetGroupAsync(groupId);
            return Ok(group);
        }

        [HttpGet]
        public async Task<ActionResult<List<GroupShortDto>>> GetGroups()
        {
            var uid = User.FindFirst(ClaimTypes.Name)?.Value;
            if(string.IsNullOrEmpty(uid))
            {
                return Unauthorized();
            }

            var groups = await groupService.GetGroupsAsync(uid);
            return Ok(groups);
        }

        [HttpPost("{groupId}/Students")]
        public async Task<ActionResult<StudentDto>> AddStudent([FromRoute] string groupId, [FromBody] CreateStudentDto studentDto)
        {
            var student = await groupService.AddStudent(groupId, studentDto);
            return Created($"{Request.Host.Value}{Request.Path.Value}/{student.Id}", student);
        }

        [HttpDelete("{groupId}/Students/{studentId}")]
        public async Task<ActionResult> RemoveStudent([FromRoute] string groupId, [FromRoute] string studentId)
        {
            await groupService.RemoveStudentAsync(groupId, studentId);
            return NoContent();
        }
    }
}
