using Ejournal.Application.Dtos;
using Ejournal.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Ejournal.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class JournalsController(IJournalService journalService) : ControllerBase
    {
        [HttpPost]
        public async Task<ActionResult<JournalDto>> CreateJournal([FromBody] CreateJournalDto journalDto)
        {
            var uid = User.FindFirst(ClaimTypes.Name)?.Value;
            if (string.IsNullOrEmpty(uid))
            {
                return Unauthorized();
            }

            var journal = await journalService.CreateJournalAsync(journalDto, uid);
            return CreatedAtAction(nameof(GetJournal), new { journalId = journal.Id }, journal);
        }

        [HttpGet("{journalId}")]
        public async Task<ActionResult<JournalDto>> GetJournal([FromRoute] string journalId)
        {
            var journal = await journalService.GetJournalAsync(journalId);
            return Ok(journal);
        }

        [HttpGet]
        public async Task<ActionResult<List<JournalShortDto>>> GetJournals()
        {
            var uid = User.FindFirst(ClaimTypes.Name)?.Value;
            if (string.IsNullOrEmpty(uid))
            {
                return Unauthorized();
            }

            var journals = await journalService.GetJournalsAsync(uid);
            return Ok(journals);
        }

        [HttpPost("{journalId}/Dates")]
        public async Task<ActionResult> AddAbsenceDate([FromRoute] string journalId, [FromBody] AbsenceDateDto absenceDto)
        {
            await journalService.AddAbsenceDateAsync(journalId, absenceDto.AbsenceDate);
            return Created();
        }

        [HttpDelete("{journalId}/Dates/{absenceDate}")]
        public async Task<ActionResult> RemoveAbsenceDate([FromRoute] string journalId, [FromRoute] string absenceDate)
        {
            await journalService.RemoveAbsenceDateAsync(journalId, absenceDate);
            return NoContent();
        }

        [HttpPost("{journalId}/Dates/{absenceDate}/Absences")]
        public async Task<ActionResult> MarkStudentAbsentDate([FromRoute] string journalId, [FromRoute] string absenceDate, [FromBody] StudentAbsenceDto absenceDto)
        {
            await journalService.MarkStudentAbsentAsync(journalId, absenceDto.StudentId, absenceDate);
            return Created();
        }

        [HttpDelete("{journalId}/Dates/{absenceDate}/Absences/{studentId}")]
        public async Task<ActionResult> UnmarkStudentAbsentDate([FromRoute] string journalId, [FromRoute] string absenceDate, [FromRoute] string studentId)
        {
            await journalService.UnmarkStudentAbsentAsync(journalId, studentId, absenceDate);
            return NoContent();
        }
    }
}
