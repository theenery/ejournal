﻿using Ejournal.Application.Dtos;
using Ejournal.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Ejournal.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class SubjectsController(ISubjectService subjectService) : ControllerBase
    {
        [HttpPost]
        public async Task<ActionResult<SubjectDto>> CreateSubject([FromBody] CreateSubjectDto subjectDto)
        {
            var uid = User.FindFirst(ClaimTypes.Name)?.Value;
            if (string.IsNullOrEmpty(uid))
            {
                return Unauthorized();
            }

            var subject = await subjectService.CreateSubjectAsync(subjectDto, uid);
            return CreatedAtAction(nameof(GetSubject), new { subjectId = subject.Id }, subject);
        }

        [HttpGet("{subjectId}")]
        public async Task<ActionResult<SubjectDto>> GetSubject([FromRoute] string subjectId)
        {
            var subject = await subjectService.GetSubjectAsync(subjectId);
            return Ok(subject);
        }

        [HttpGet]
        public async Task<ActionResult<List<SubjectDto>>> GetSubjects()
        {
            var uid = User.FindFirst(ClaimTypes.Name)?.Value;
            if (string.IsNullOrEmpty(uid))
            {
                return Unauthorized();
            }

            var subjects = await subjectService.GetSubjectsAsync(uid);
            return Ok(subjects);
        }
    }
}
