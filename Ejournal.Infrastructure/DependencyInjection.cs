﻿using Ejournal.Domain.Interfaces;
using Ejournal.Infrastructure.Repositories;
using Google.Cloud.Firestore;
using Microsoft.Extensions.DependencyInjection;

namespace Ejournal.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            Environment.SetEnvironmentVariable(
                "GOOGLE_APPLICATION_CREDENTIALS",
                @"firebase-key.json");

            services
                .AddSingleton(FirestoreDb.Create("ejournal-4920f"));

            services
                .AddTransient<IGroupRepository, GroupRepository>()
                .AddTransient<IJournalRepository, JournalRepository>()
                .AddTransient<ISubjectRepository, SubjectRepository>();

            return services;
        }
    }
}
