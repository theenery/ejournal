﻿using Google.Cloud.Firestore;

namespace Ejournal.Infrastructure.Documents
{
    [FirestoreData]
    public class GroupDocument
    {
        [FirestoreDocumentId]
        public required string Id { get; set; }

        [FirestoreProperty]
        public required string OwnerId { get; set; }

        [FirestoreProperty]
        public required string Name { get; set; }

        [FirestoreProperty]
        public required List<StudentDocument> Students { get; set; }
    }
}
