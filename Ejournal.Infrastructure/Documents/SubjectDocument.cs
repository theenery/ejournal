﻿using Google.Cloud.Firestore;

namespace Ejournal.Infrastructure.Documents
{
    [FirestoreData]
    public class SubjectDocument
    {
        [FirestoreDocumentId]
        public required string Id { get; set; }

        [FirestoreProperty]
        public required string OwnerId { get; set; }

        [FirestoreProperty]
        public required string Name { get; set; }
    }
}
