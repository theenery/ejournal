﻿using Google.Cloud.Firestore;

namespace Ejournal.Infrastructure.Documents
{
    [FirestoreData]
    public class JournalDocument
    {
        [FirestoreDocumentId]
        public required string Id { get; set; }

        [FirestoreProperty]
        public required string OwnerId { get; set; }

        [FirestoreProperty]
        public required string GroupId { get; set; }

        [FirestoreProperty]
        public required string SubjectId { get; set; }

        [FirestoreProperty]
        public required Dictionary<string, List<string>> Absences { get; set; }
    }
}
