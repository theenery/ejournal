﻿using Google.Cloud.Firestore;

namespace Ejournal.Infrastructure.Documents
{
    [FirestoreData]
    public class StudentDocument
    {
        [FirestoreProperty]
        public required string Id { get; set; }

        [FirestoreProperty]
        public required string FirstName { get; set; }

        [FirestoreProperty]
        public required string LastName { get; set; }

        [FirestoreProperty]
        public string? MiddleName { get; set; }
    }
}
