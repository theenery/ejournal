﻿using Ejournal.Domain.Entities;
using Ejournal.Domain.Interfaces;
using Ejournal.Domain.ValueObjects;
using Ejournal.Infrastructure.Documents;
using Google.Cloud.Firestore;
using Newtonsoft.Json;

namespace Ejournal.Infrastructure.Repositories
{
    public class GroupRepository(FirestoreDb db) : IGroupRepository
    {
        private const string _collectionName = "Groups";

        public async Task<GroupEntity?> AddAsync(GroupEntity group)
        {
            var collection = db.Collection(_collectionName);
            var groupDocument = new GroupDocument
            {
                Id = group.Id,
                OwnerId = group.OwnerId,
                Name = group.Name,
                Students = group.Students.Select(svo => new StudentDocument
                {
                    Id = svo.Id,
                    FirstName = svo.FirstName,
                    LastName = svo.LastName,
                    MiddleName = svo.MiddleName
                }).ToList()
            };

            var docRef = await collection.AddAsync(groupDocument);
            var snapshot = await docRef.GetSnapshotAsync();
            if (!snapshot.Exists) return null;

            groupDocument = snapshot.ConvertTo<GroupDocument>();
            return new GroupEntity(groupDocument.Id, groupDocument.OwnerId, groupDocument.Name, groupDocument.Students.Select(sd => new StudentValueObject(sd.Id, sd.FirstName, sd.LastName, sd.MiddleName)).ToList());
        }

        public async Task<GroupEntity?> GetByIdAsync(string id)
        {
            var collection = db.Collection(_collectionName);
            var docRef = collection.Document(id);
            var snapshot = await docRef.GetSnapshotAsync();
            if (!snapshot.Exists) return null;

            var groupDocument = snapshot.ConvertTo<GroupDocument>();
            return new GroupEntity(groupDocument.Id, groupDocument.OwnerId, groupDocument.Name, groupDocument.Students.Select(sd => new StudentValueObject(sd.Id, sd.FirstName, sd.LastName,sd.MiddleName)).ToList());
        }

        public async Task<List<GroupEntity>> GetByOwnerIdAsync(string ownerId)
        {
            var collection = db.Collection(_collectionName);
            var query = collection.WhereEqualTo("OwnerId", ownerId);
            var snapshot = await query.GetSnapshotAsync();

            var groupEntities = new List<GroupEntity>();
            foreach (var document in snapshot.Documents)
            {
                if (document.Exists)
                {
                    var groupDocument = document.ConvertTo<GroupDocument>();
                    var groupEntity = new GroupEntity(groupDocument.Id, groupDocument.OwnerId, groupDocument.Name, groupDocument.Students.Select(sd => new StudentValueObject(sd.Id, sd.FirstName, sd.LastName, sd.MiddleName)).ToList());
                    groupEntities.Add(groupEntity);
                }
            }

            return groupEntities;
        }

        public async Task UpdateAsync(GroupEntity group)
        {
            var collection = db.Collection(_collectionName);
            var docRef = collection.Document(group.Id);
            var groupDocument = new GroupDocument
            {
                Id = group.Id,
                OwnerId = group.OwnerId,
                Name = group.Name,
                Students = group.Students.Select(svo => new StudentDocument
                {
                    Id = svo.Id,
                    FirstName = svo.FirstName,
                    LastName = svo.LastName,
                    MiddleName = svo.MiddleName
                }).ToList()
            };

            await docRef.SetAsync(groupDocument, SetOptions.Overwrite);
        }
    }
}
