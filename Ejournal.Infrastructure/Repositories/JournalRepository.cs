﻿using Ejournal.Domain.Entities;
using Ejournal.Domain.Interfaces;
using Ejournal.Domain.ValueObjects;
using Ejournal.Infrastructure.Documents;
using Google.Cloud.Firestore;

namespace Ejournal.Infrastructure.Repositories
{
    public class JournalRepository(FirestoreDb db) : IJournalRepository
    {
        private const string _collectionName = "Journals";

        public async Task<JournalEntity?> AddAsync(JournalEntity journal)
        {
            var collection = db.Collection(_collectionName);
            var journalDocument = new JournalDocument
            {
                Id = journal.Id,
                OwnerId = journal.OwnerId,
                Absences = journal.Absences.ToDictionary(),
                GroupId = journal.GroupId,
                SubjectId = journal.SubjectId
            };

            var docRef = await collection.AddAsync(journalDocument);
            var snapshot = await docRef.GetSnapshotAsync();
            if (!snapshot.Exists) return null;

            journalDocument = snapshot.ConvertTo<JournalDocument>();
            return new JournalEntity(journalDocument.Id, journalDocument.OwnerId, journalDocument.GroupId, journalDocument.SubjectId, journalDocument.Absences);
        }

        public async Task DeleteAsync(string id)
        {
            var collection = db.Collection(_collectionName);
            var docRef = collection.Document(id);

            await docRef.DeleteAsync();
        }

        public async Task<JournalEntity?> GetByIdAsync(string id)
        {
            var collection = db.Collection(_collectionName);
            var docRef = collection.Document(id);
            var snapshot = await docRef.GetSnapshotAsync();
            if (!snapshot.Exists) return null;

            var journalDocument = snapshot.ConvertTo<JournalDocument>();
            return new JournalEntity(journalDocument.Id, journalDocument.OwnerId, journalDocument.GroupId, journalDocument.SubjectId, journalDocument.Absences);
        }

        public async Task<List<JournalEntity>> GetByOwnerIdAsync(string ownerId)
        {
            var collection = db.Collection(_collectionName);
            var query = collection.WhereEqualTo("OwnerId", ownerId);
            var snapshot = await query.GetSnapshotAsync();

            var journalEntities = new List<JournalEntity>();
            foreach (var document in snapshot.Documents)
            {
                if (document.Exists)
                {
                    var journalDocument = document.ConvertTo<JournalDocument>();
                    var journalEntity = new JournalEntity(journalDocument.Id, journalDocument.OwnerId, journalDocument.GroupId, journalDocument.SubjectId, journalDocument.Absences);
                    journalEntities.Add(journalEntity);
                }
            }

            return journalEntities;
        }

        public async Task UpdateAsync(JournalEntity journal)
        {
            var collection = db.Collection(_collectionName);
            var docRef = collection.Document(journal.Id);
            var journalDocument = new JournalDocument
            {
                Id = journal.Id,
                OwnerId = journal.OwnerId,
                GroupId = journal.GroupId,
                SubjectId = journal.SubjectId,
                Absences = journal.Absences.ToDictionary()
            };

            await docRef.SetAsync(journalDocument, SetOptions.Overwrite);
        }
    }
}
