﻿using Ejournal.Domain.Entities;
using Ejournal.Domain.Interfaces;
using Ejournal.Domain.ValueObjects;
using Ejournal.Infrastructure.Documents;
using Google.Cloud.Firestore;

namespace Ejournal.Infrastructure.Repositories
{
    public class SubjectRepository(FirestoreDb db) : ISubjectRepository
    {
        private const string _collectionName = "Subjects";

        public async Task<SubjectEntity?> AddAsync(SubjectEntity subject)
        {
            var collection = db.Collection(_collectionName);
            var subjectDocument = new SubjectDocument
            {
                Id = subject.Id,
                OwnerId = subject.OwnerId,
                Name = subject.Name
            };

            var docRef = await collection.AddAsync(subjectDocument);
            var snapshot = await docRef.GetSnapshotAsync();
            if (!snapshot.Exists) return null;

            subjectDocument = snapshot.ConvertTo<SubjectDocument>();
            return new SubjectEntity(subjectDocument.Id, subjectDocument.OwnerId, subjectDocument.Name);
        }

        public async Task<SubjectEntity?> GetByIdAsync(string id)
        {
            var collection = db.Collection(_collectionName);
            var docRef = collection.Document(id);
            var snapshot = await docRef.GetSnapshotAsync();
            if (!snapshot.Exists) return null;

            var subjectDocument = snapshot.ConvertTo<SubjectDocument>();
            return new SubjectEntity(subjectDocument.Id, subjectDocument.OwnerId, subjectDocument.Name);
        }

        public async Task<List<SubjectEntity>> GetByOwnerIdAsync(string ownerId)
        {
            var collection = db.Collection(_collectionName);
            var query = collection.WhereEqualTo("OwnerId", ownerId);
            var snapshot = await query.GetSnapshotAsync();

            var subjectEntities = new List<SubjectEntity>();
            foreach (var document in snapshot.Documents)
            {
                if (document.Exists)
                {
                    var subjectDocument = document.ConvertTo<SubjectDocument>();
                    var subjectEntity = new SubjectEntity(subjectDocument.Id, subjectDocument.OwnerId, subjectDocument.Name);
                    subjectEntities.Add(subjectEntity);
                }
            }

            return subjectEntities;
        }

        public async Task UpdateAsync(SubjectEntity subject)
        {
            var collection = db.Collection(_collectionName);
            var docRef = collection.Document(subject.Id);
            var subjectDocument = new SubjectDocument
            {
                Id = subject.Id,
                OwnerId = subject.OwnerId,
                Name = subject.Name
            };

            await docRef.SetAsync(subjectDocument, SetOptions.Overwrite);
        }
    }
}
