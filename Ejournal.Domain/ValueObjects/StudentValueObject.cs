﻿namespace Ejournal.Domain.ValueObjects
{
    public class StudentValueObject
    {
        public string Id { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string? MiddleName { get; private set; }

        public StudentValueObject(string id, string firstName, string lastName, string? middleName)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            MiddleName = middleName;
        }
    }
}
