﻿using Ejournal.Domain.ValueObjects;

namespace Ejournal.Domain.Entities
{
    public class GroupEntity
    {
        private List<StudentValueObject> _students;

        public string Id { get; private set; } = null!;
        public string OwnerId { get; private set; } = null!;
        public string Name { get; private set; } = null!;
        public IReadOnlyCollection<StudentValueObject> Students => _students.AsReadOnly();

        public GroupEntity(string id, string ownerId, string name, List<StudentValueObject> students)
        {
            Id = id;
            OwnerId = ownerId;
            Name = name;
            _students = students;
        }

        public void AddStudent(StudentValueObject student)
        {
            if (_students.Contains(student))
            {
                throw new InvalidOperationException("Student is already in group.");
            }

            _students.Add(student);
        }

        public void RemoveStudent(string studentId)
        {
            _students = _students.Where(s => s.Id != studentId).ToList();
        }
    }
}
