﻿namespace Ejournal.Domain.Entities
{
    public class SubjectEntity
    {
        public string Id { get; private set; }
        public string OwnerId { get; private set; } = null!;
        public string Name { get; private set; }

        public SubjectEntity(string id, string ownerId, string name)
        {
            Id = id;
            OwnerId = ownerId;
            Name = name;
        }
    }
}
