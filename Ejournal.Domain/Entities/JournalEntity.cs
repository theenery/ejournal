﻿namespace Ejournal.Domain.Entities
{
    public class JournalEntity
    {
        private Dictionary<string, List<string>> _absences;

        public string Id { get; private set; } = null!;
        public string OwnerId { get; private set; } = null!;
        public string GroupId { get; private set; } = null!;
        public string SubjectId { get; private set; } = null!;
        public IReadOnlyDictionary<string, List<string>> Absences => _absences.AsReadOnly();

        public JournalEntity(string id, string ownerId, string groupId, string subjectId, Dictionary<string, List<string>> abcences)
        {
            Id = id;
            OwnerId = ownerId;
            _absences = abcences;
            GroupId = groupId;
            SubjectId = subjectId;
        }

        public void AddAbsenceDate(string date)
        {
            if (_absences.ContainsKey(date))
            {
                throw new InvalidOperationException("Date is already in journal.");
            }

            _absences.Add(date, []);
        }

        public void RemoveAbsenceDate(string date)
        {
            _absences.Remove(date);
        }

        public void MarkAbsent(string studentId, string date) {
            if (!_absences.TryGetValue(date, out List<string>? absentStudentIds))
            {
                throw new InvalidOperationException("Date is not already in journal.");
            }

            if (absentStudentIds.Contains(studentId))
            {
                throw new InvalidOperationException("Student is already absent.");
            }

            absentStudentIds.Add(studentId);
        }

        public void UnmarkAbsent(string studentId, string date)
        {
            if (!_absences.TryGetValue(date, out List<string>? absentStudentIds))
            {
                throw new InvalidOperationException("Date is not already in journal.");
            }

            absentStudentIds.Remove(studentId);
        }
    }
}
