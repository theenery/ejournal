﻿using Ejournal.Domain.Entities;

namespace Ejournal.Domain.Interfaces
{
    public interface ISubjectRepository
    {
        public Task<SubjectEntity?> AddAsync(SubjectEntity subject);
        public Task<SubjectEntity?> GetByIdAsync(string id);
        public Task<List<SubjectEntity>> GetByOwnerIdAsync(string ownerId);
        public Task UpdateAsync(SubjectEntity subject);
    }
}
