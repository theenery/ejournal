﻿using Ejournal.Domain.Entities;

namespace Ejournal.Domain.Interfaces
{
    public interface IJournalRepository
    {
        public Task<JournalEntity?> AddAsync(JournalEntity journal);
        public Task DeleteAsync(string id);
        public Task<JournalEntity?> GetByIdAsync(string id);
        public Task<List<JournalEntity>> GetByOwnerIdAsync(string ownerId);
        public Task UpdateAsync(JournalEntity journal);
    }
}
