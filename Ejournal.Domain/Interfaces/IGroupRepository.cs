﻿using Ejournal.Domain.Entities;

namespace Ejournal.Domain.Interfaces
{
    public interface IGroupRepository
    {
        public Task<GroupEntity?> AddAsync(GroupEntity group);
        public Task<GroupEntity?> GetByIdAsync(string id);
        public Task<List<GroupEntity>> GetByOwnerIdAsync(string ownerId);
        public Task UpdateAsync(GroupEntity group);
    }
}
