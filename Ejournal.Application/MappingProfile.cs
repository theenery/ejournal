﻿using AutoMapper;
using Ejournal.Application.Dtos;
using Ejournal.Domain.Entities;
using Ejournal.Domain.ValueObjects;

namespace Ejournal.Application
{
    public class MappingProfile : Profile
    {
        public MappingProfile() 
        { 
            //CreateMap<GroupDto, GroupEntity>();
            CreateMap<GroupEntity, GroupDto>();
            
            //CreateMap<JournalDto, JournalEntity>();
            CreateMap<JournalEntity, JournalDto>();

            //CreateMap<StudentDto, StudentValueObject>();
            CreateMap<StudentValueObject, StudentDto>();

            //CreateMap<SubjectDto, SubjectEntity>();
            CreateMap<SubjectEntity, SubjectDto>();


            CreateMap<GroupEntity, GroupShortDto>();


            //CreateMap<CreateGroupDto, GroupEntity>();
            //CreateMap<CreateJournalDto, JournalEntity>();
            //CreateMap<CreateStudentDto, StudentValueObject>();
            //CreateMap<CreateSubjectDto, SubjectEntity>();
        }
    }
}
