﻿using AutoMapper;
using Ejournal.Application.Dtos;
using Ejournal.Application.Interfaces;
using Ejournal.Domain.Entities;
using Ejournal.Domain.Interfaces;

namespace Ejournal.Application.Services
{
    public class SubjectService(IMapper mapper, ISubjectRepository subjectRepository) : ISubjectService
    {
        public async Task<SubjectDto> CreateSubjectAsync(CreateSubjectDto subjectDto, string ownerId)
        {
            var subject = await subjectRepository.AddAsync(new SubjectEntity("", ownerId, subjectDto.Name))
                ?? throw new InvalidOperationException("Cannot create subject.");

            return mapper.Map<SubjectDto>(subject);
        }

        public async Task<SubjectDto> GetSubjectAsync(string id)
        {
            var subject = await subjectRepository.GetByIdAsync(id)
                ?? throw new InvalidOperationException("There is no such subject.");

            return mapper.Map<SubjectDto>(subject);
        }


        public async Task<List<SubjectDto>> GetSubjectsAsync(string ownerId)
        {
            var subjects = await subjectRepository.GetByOwnerIdAsync(ownerId);

            return mapper.Map<List<SubjectDto>>(subjects);
        }
    }
}
