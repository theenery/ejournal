﻿using AutoMapper;
using Ejournal.Application.Dtos;
using Ejournal.Application.Interfaces;
using Ejournal.Domain.Entities;
using Ejournal.Domain.Interfaces;

namespace Ejournal.Application.Services
{
    public class JournalService(IMapper mapper, IJournalRepository journalRepository, IGroupRepository groupRepository, ISubjectRepository subjectRepository) : IJournalService
    {
        public async Task AddAbsenceDateAsync(string journalId, string date)
        {
            var journal = await journalRepository.GetByIdAsync(journalId)
                ?? throw new InvalidOperationException("There is no such journal.");
            journal.AddAbsenceDate(date);
            await journalRepository.UpdateAsync(journal);
        }

        public async Task<JournalDto> CreateJournalAsync(CreateJournalDto journalDto, string ownerId)
        {
            var journal = new JournalEntity("", ownerId, journalDto.GroupId, journalDto.SubjectId, []);
            journal = await journalRepository.AddAsync(journal)
                ?? throw new InvalidOperationException("Cannot create journal.");

            return await GetJournalAsync(journal.Id);
        }

        public async Task<JournalDto> GetJournalAsync(string journalId)
        {
            var journal = await journalRepository.GetByIdAsync(journalId)
                ?? throw new InvalidOperationException("There is no such journal.");
            var group = await groupRepository.GetByIdAsync(journal.GroupId)
                ?? throw new InvalidOperationException("There is no such group.");
            var subject = await subjectRepository.GetByIdAsync(journal.SubjectId)
                ?? throw new InvalidOperationException("There is no such subject.");

            return new JournalDto
            {
                Id = journal.Id,
                OwnerId = journal.OwnerId,
                Absences = journal.Absences.ToDictionary(),
                Group = new GroupDto
                {
                    Id = group.Id,
                    OwnerId = group.OwnerId,
                    Name = group.Name,
                    Students = group.Students.Select(svo => new StudentDto
                    {
                        Id = svo.Id,
                        FirstName = svo.FirstName,
                        LastName = svo.LastName,
                        MiddleName = svo.MiddleName
                    }).ToList()
                },
                Subject = new SubjectDto
                {
                    Id = subject.Id,
                    OwnerId = subject.OwnerId,
                    Name = subject.Name
                }
            };
        }

        public async Task<List<JournalShortDto>> GetJournalsAsync(string ownerId)
        {
            var journals = await journalRepository.GetByOwnerIdAsync(ownerId);

            return journals.Select(async journal =>
            {
                var group = await groupRepository.GetByIdAsync(journal.GroupId)
                    ?? throw new InvalidOperationException("There is no such group.");
                var subject = await subjectRepository.GetByIdAsync(journal.SubjectId)
                    ?? throw new InvalidOperationException("There is no such subject.");

                return new JournalShortDto
                {
                    Id = journal.Id,
                    Group = new GroupShortDto
                    {
                        Id = group.Id,
                        OwnerId = group.OwnerId,
                        Name = group.Name
                    },
                    Subject = new SubjectDto
                    {
                        Id = subject.Id,
                        OwnerId = subject.OwnerId,
                        Name = subject.Name
                    }
                };
            }).Select(t => t.Result).ToList();
        }

        public async Task MarkStudentAbsentAsync(string journalId, string studentId, string date)
        {
            var journal = await journalRepository.GetByIdAsync(journalId)
                ?? throw new InvalidOperationException("There is no such journal.");
            journal.MarkAbsent(studentId, date);
            await journalRepository.UpdateAsync(journal);
        }

        public async Task RemoveAbsenceDateAsync(string journalId, string date)
        {
            var journal = await journalRepository.GetByIdAsync(journalId)
                ?? throw new InvalidOperationException("There is no such journal.");
            journal.RemoveAbsenceDate(date);
            await journalRepository.UpdateAsync(journal);
        }

        public async Task UnmarkStudentAbsentAsync(string journalId, string studentId, string date)
        {
            var journal = await journalRepository.GetByIdAsync(journalId)
                ?? throw new InvalidOperationException("There is no such journal.");
            journal.UnmarkAbsent(studentId, date);
            await journalRepository.UpdateAsync(journal);
        }
    }
}
