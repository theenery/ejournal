﻿using AutoMapper;
using Ejournal.Application.Dtos;
using Ejournal.Application.Interfaces;
using Ejournal.Domain.Entities;
using Ejournal.Domain.Interfaces;
using Ejournal.Domain.ValueObjects;

namespace Ejournal.Application.Services
{
    public class GroupService(IMapper mapper, IGroupRepository groupRepository) : IGroupService
    {
        public async Task<StudentDto> AddStudent(string groupId, CreateStudentDto studentDto)
        {
            var group = await groupRepository.GetByIdAsync(groupId)
                ?? throw new InvalidOperationException("There is no such group.");
            var student = new StudentValueObject(Guid.NewGuid().GetHashCode().ToString(), studentDto.FirstName, studentDto.LastName, studentDto.MiddleName);
            
            group.AddStudent(student);
            await groupRepository.UpdateAsync(group);

            return mapper.Map<StudentDto>(student);
        }

        public async Task<GroupDto> CreateGroupAsync(CreateGroupDto groupDto, string ownerId)
        {
            var group = await groupRepository.AddAsync(new GroupEntity("", ownerId, groupDto.Name, []))
                ?? throw new InvalidOperationException("Cannot create group.");

            return mapper.Map<GroupDto>(group);
        }

        public async Task<GroupDto> GetGroupAsync(string groupId)
        {
            var group = await groupRepository.GetByIdAsync(groupId)
                ?? throw new InvalidOperationException("There is no such group.");

            return mapper.Map<GroupDto>(group);
        }

        public async Task<List<GroupShortDto>> GetGroupsAsync(string ownerId)
        {
            var groups = await groupRepository.GetByOwnerIdAsync(ownerId);

            return mapper.Map<List<GroupShortDto>>(groups);
        }

        public async Task RemoveStudentAsync(string groupId, string studentId)
        {
            var group = await groupRepository.GetByIdAsync(groupId)
                ?? throw new InvalidOperationException("There is no such group.");

            group.RemoveStudent(studentId);
            await groupRepository.UpdateAsync(group);
        }
    }
}
