﻿namespace Ejournal.Application.Dtos
{
    public class JournalDto
    {
        public required string Id { get; set; }

        public required string OwnerId { get; set; }

        public required GroupDto Group { get; set; }

        public required SubjectDto Subject { get; set; }

        public required Dictionary<string, List<string>> Absences { get; set; }
    }
}
