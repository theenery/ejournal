﻿namespace Ejournal.Application.Dtos
{
    public class SubjectDto
    {
        public required string Id { get; set; }

        public required string OwnerId { get; set; }

        public required string Name { get; set; }
    }
}
