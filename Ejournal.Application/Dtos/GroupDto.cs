﻿namespace Ejournal.Application.Dtos
{
    public class GroupDto
    {
        public required string Id { get; set; }

        public required string OwnerId { get; set; }

        public required string Name { get; set; }

        public required List<StudentDto> Students { get; set; }
    }
}
