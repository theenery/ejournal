﻿namespace Ejournal.Application.Dtos
{
    public class CreateSubjectDto
    {
        public required string Name { get; set; }
    }
}
