﻿namespace Ejournal.Application.Dtos
{
    public class JournalShortDto
    {
        public required string Id { get; set; }

        public required GroupShortDto Group { get; set; }

        public required SubjectDto Subject { get; set; }
    }
}
