﻿namespace Ejournal.Application.Dtos
{
    public class StudentAbsenceDto
    {
        public required string StudentId {  get; set; }
    }
}
