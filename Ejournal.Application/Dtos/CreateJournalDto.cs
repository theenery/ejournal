﻿namespace Ejournal.Application.Dtos
{
    public class CreateJournalDto
    {
        public required string GroupId { get; set; }

        public required string SubjectId { get; set; }
    }
}
