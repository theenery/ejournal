﻿namespace Ejournal.Application.Dtos
{
    public class CreateGroupDto
    {
        public required string Name {  get; set; }
    }
}
