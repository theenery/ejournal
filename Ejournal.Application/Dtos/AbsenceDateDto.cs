﻿namespace Ejournal.Application.Dtos
{
    public class AbsenceDateDto
    {
        public required string AbsenceDate {  get; set; }
    }
}
