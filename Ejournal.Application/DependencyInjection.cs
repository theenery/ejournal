﻿using Ejournal.Application.Interfaces;
using Ejournal.Application.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Ejournal.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services
                .AddAutoMapper(cfg => cfg.AddProfile<MappingProfile>())

                .AddTransient<IGroupService, GroupService>()
                .AddTransient<IJournalService, JournalService>()
                .AddTransient<ISubjectService, SubjectService>();

            return services;
        }
    }
}
