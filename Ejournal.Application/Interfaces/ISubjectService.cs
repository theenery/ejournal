﻿using Ejournal.Application.Dtos;

namespace Ejournal.Application.Interfaces
{
    public interface ISubjectService
    {
        public Task<SubjectDto> CreateSubjectAsync(CreateSubjectDto subjectDto, string ownerId);
        public Task<SubjectDto> GetSubjectAsync(string id);
        public Task<List<SubjectDto>> GetSubjectsAsync(string ownerId);
    }
}
