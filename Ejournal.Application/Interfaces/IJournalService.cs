﻿using Ejournal.Application.Dtos;

namespace Ejournal.Application.Interfaces
{
    public interface IJournalService
    {
        public Task AddAbsenceDateAsync(string journalId, string date);
        public Task<JournalDto> CreateJournalAsync(CreateJournalDto journalDto, string ownerId);
        public Task<JournalDto> GetJournalAsync(string journalId);
        public Task<List<JournalShortDto>> GetJournalsAsync(string ownerId);
        public Task MarkStudentAbsentAsync(string journalId, string studentId, string date);
        public Task RemoveAbsenceDateAsync(string journalId, string date);
        public Task UnmarkStudentAbsentAsync(string journalId, string studentId, string date);
    }
}
