﻿using Ejournal.Application.Dtos;

namespace Ejournal.Application.Interfaces
{
    public interface IGroupService
    {
        public Task<StudentDto> AddStudent(string groupId, CreateStudentDto studentDto);
        public Task<GroupDto> CreateGroupAsync(CreateGroupDto groupDto, string ownerId);
        public Task<GroupDto> GetGroupAsync(string groupId);
        public Task<List<GroupShortDto>> GetGroupsAsync(string ownerId);
        public Task RemoveStudentAsync(string groupId, string studentId);
    }
}
